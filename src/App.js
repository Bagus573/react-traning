import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Header from './Temp/header';
import Footer from './Temp/footer';
import Mainmenu from './Temp/mainMenu';

import Lifecycle from './Content/Lifecycle';
import Form from './Content/Form';
import Redux from './Content/Redux/Index';

function Reduxs() {
  return <Redux name="Redux" />
}

function Lifecycles() {
  return <Lifecycle name="Lifecycle" />
}

function Forms() {
  return <Form name="Form" />
}

function Topics() {
  return <h1>Topics</h1>
}

function Notfound() {
  return <h1>404</h1>
}

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Header />
        <BrowserRouter>
          <aside className="main-sidebar sidebar-dark-primary elevation-4">
            <a href="index3.html" className="brand-link">
              <img src="assets/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" className="brand-image img-circle elevation-3" />
              <span className="brand-text font-weight-light">AdminLTE 3</span>
            </a>
            <div className="sidebar">
              <nav className="mt-2">
                <Mainmenu />
              </nav>
            </div>
          </aside>
          <div className="content-wrapper">
            <Switch>
              <Route path='/topics' exact component={Topics} />
              <Route path='/Lifecycles' exact component={Lifecycles} />
              <Route path='/redux' exact component={Reduxs} />
              <Route path='/form' exact component={Forms} />
              <Route component={Notfound} />
            </Switch>
          </div>
        </BrowserRouter>
        <Footer />
      </React.Fragment>
    );
  }
}

export default App;
import React, { Component } from 'react';
import '../App.css';

class Divstyle extends Component {


    render() { 
        const { name, color } = this.props;

        const style = {
            margin: 20,
            padding: 20,
            color: 'white',
            background: color
        }

        return (
            <div style={style}>
                {name}
            </div >
        )
    }
}

export default Divstyle;
import React, { Component } from 'react';

class RowTable extends Component {
    renderBodyRow() {
        const { body } = this.props;
        if (body) {
            return (
                <React.Fragment>
                    {
                        body.map(
                            (data, index) => <tr key={index}>{this.renderRow(index)}</tr>
                        )
                    }
                </React.Fragment>
            )
        }
    };

    renderRow(Number) {
        const { body } = this.props;
        if (body[Number]) {
            return (
                <React.Fragment>
                    <td>{Number+1}</td>
                    {
                        body[Number].map(
                            (data, index) => <td key={index}>{data}</td>
                        )
                    }
                </React.Fragment>
            )
        }
    }

    render() {
        this.renderBodyRow = this.renderBodyRow.bind(this);

        return (
            <React.Fragment>
                { this.renderBodyRow() }
            </React.Fragment>
        )
    }
}

export default RowTable;
import React, { Component } from 'react';
import '../App.css';

class App extends Component {

    render() {
        const { name } = this.props;
        return (
            <div className="content-header">
                <div className="container-fluid">
                    <div className="row mb-2">
                        <div className="col-sm-6">
                            <h1 className="m-0 text-dark">{name}</h1>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default App;
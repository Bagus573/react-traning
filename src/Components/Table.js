import React, { Component } from 'react';
import Row from './Table/row';
import '../App.css';

class Table extends Component {

    headingRender() {
        const { headings } = this.props;
        if (headings) {
            return (
                <tr>
                    {
                        headings.map(
                            (data, index) => <td key={index}><b>{data}</b></td>
                        )
                    }
                </tr>
            )
        }
    }

    render() {
        const { body } = this.props;

        return (
            <section className="content">
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-header">
                                <h3 className="card-title">DataTable with minimal features & hover style</h3>
                            </div>
                            <div className="card-body">
                                <table id="example1" className="table table-bordered table-hover">
                                    <thead>
                                        {this.headingRender()}
                                    </thead>
                                    <tbody>
                                        <Row body={body} />
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Table;
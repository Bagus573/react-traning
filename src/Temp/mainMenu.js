import React, { Component } from 'react';
import { Link } from "react-router-dom";

function Menustyle(props) {
    return (
        <li className="nav-item">
            <Link to={props.link} className="nav-link">
                <i className={props.class}></i>
                <p>{props.name}</p>
            </Link>
        </li>
    )
}

class Menus extends Component {
    render() {
        return (
            <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <Menustyle link="/topics" className="nav-icon fas fa-tachometer-alt" name="Topics" />
                <Menustyle link="/lifecycles" className="nav-icon fa fa-plus" name="Lifecycles" />
                <Menustyle link="/redux" className="nav-icon fa fa-list" name="Redux" />
                <Menustyle link="/form" className="nav-icon fa fa-list" name="Form" />
            </ul>
        );
    }
}

export default Menus;
import React, { Component } from 'react';

class Header extends Component {
    render() {
        return (
            <nav className="main-header navbar navbar-expand navbar-white navbar-light">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <div className="nav-link">
                            <i className="fa fa-dashboard"></i>
                        </div>
                    </li>
                </ul>
            </nav>
        );
    }
}

export default Header;
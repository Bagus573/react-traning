import React, { Component } from 'react';

import Loading from '../Components/loading';
import Table from '../Components/Table';
import Headercontent from '../Components/HeaderContent';
import '../App.css';

class Content extends Component {

  constructor(props) {
    super(props)
    this.state = {
      items: [],
      tr: [],
      isLoading: true
    }
  }

  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/todos")
      .then(response => response.json())
      .then(data => this.setState({
        items: data,
        isLoading: false
      }, () => {
        this.items();
      }))
  }

  items() {
    const { items } = this.state;
    let Item = [];

    items.forEach((data, index)=> {
      let row = [
        data.title,
        data.title, 
      ];
      Item.push(row);
    })

    this.setState({
      Tr: Item
    })
  }

  render() {
    const { Tr, isLoading } = this.state;
    const { name } = this.props;
    const headings = [
      'No',
      'Title',
      'Completed',
    ];

    if (isLoading) {
      return <Loading />
    }

    return (
      <div>
        <Headercontent name={name}/>
        <Table headings={headings} body={Tr} />
      </div>
    )
  }
}

export default Content;
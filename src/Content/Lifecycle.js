import React, { Component } from 'react';
import Devstyle from '../Components/Devstyle';

class Div extends Component {
  
  componentWillUnmount() {
  }

  render() {
    return (
      <div>
        <Devstyle color="blue" name="Biru"></Devstyle>
        <Devstyle color="red" name="Merah"></Devstyle>
        <Devstyle color="yellow" name="Kuning"></Devstyle>
      </div>
    )
  }
}

class Content extends Component {
  

  constructor(props) {
    super(props);
    this.state = {
      show: true,
      favoritecolor: "red",
      favoritecolorupdate: "red"
    };
  }

  delComponent = () => {
    this.setState({
      show: false
    })
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        favoritecolorupdate: "yellow"
      })
    }, 1000)
  }

  changeColor = () => {
    this.setState({
      favoritecolorupdate: "blue"
    });
  }

  shouldComponentUpdate() {
    return true;
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    document.getElementById("div1").innerHTML =
      "Before the update, the favorite was " + prevState.favoritecolorupdate;
  }
  componentDidUpdate() {
    document.getElementById("div2").innerHTML =
      "The updated favorite is " + this.state.favoritecolorupdate;
  }

  render() {
    let div;
    if (this.state.show) {
      div = <Div />
    };

    return (
      <div>
        <p align="center">=== Mounted  ===</p>
        <h1>My Favorite Color is {this.state.favoritecolor}</h1>
        <p align="center">=== Updated  ===</p>
        <h1>My Favorite Color is {this.state.favoritecolorupdate}</h1>
        <button type="button" onClick={this.changeColor}>Change Color</button>
        <div id="div1"></div>
        <div id="div2"></div>
        <p align="center">=== Unmounted  ===</p>
        {div}
        <button type="button" onClick={this.delComponent}>Unmount</button>
      </div>
    )
  }
}

export default Content;
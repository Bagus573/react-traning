import React, { Component } from 'react';
import { connect } from 'react-redux';
class Post extends Component {
    render() {
        return (
            <div class="card">
                <div class="card-body">
                    <h2>Title : {this.props.post.title}</h2>
                    <p>Deskripsi : {this.props.post.message}</p>
                    <div className="control-buttons">
                        <button className="btn btn-block btn-info"
                            onClick={() => this.props.dispatch({ type: 'EDIT_POST', id: this.props.post.id })
                            }
                        >Edit</button>
                        <button className="btn btn-block btn-danger"
                            onClick={() => this.props.dispatch({ type: 'DELETE_POST', id: this.props.post.id })}
                        >Delete</button>
                    </div>
                </div>
            </div>
        );
    }
}
export default connect()(Post);
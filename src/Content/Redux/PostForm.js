import React, { Component } from 'react';
import { connect } from 'react-redux';
class PostForm extends Component {
    handleSubmit = (e) => {
        e.preventDefault();
        const title = this.getTitle.value;
        const message = this.getMessage.value;
        const data = {
            id: new Date(),
            title,
            message,
            editing: false
        }
        this.props.dispatch({
            type: 'ADD_POST',
            data
        })
        this.getTitle.value = '';
        this.getMessage.value = '';
    }
    render() {
        return (
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Quick Example</h3>
                </div>
                <form onSubmit={this.handleSubmit} >
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            <input required type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukan Title" ref={(input) => this.getTitle = input} />
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Deskriptsi</label>
                            <textarea required class="form-control" rows="5" ref={(input) => this.getMessage = input}></textarea>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}
export default connect()(PostForm);
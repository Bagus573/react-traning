import React, { Component } from 'react';
import { connect } from 'react-redux';


class EditComponent extends Component {
    handleEdit = (e) => {
        e.preventDefault();
        const newTitle = this.getTitle.value;
        const newMessage = this.getMessage.value;
        const data = {
            newTitle,
            newMessage
        }
        this.props.dispatch({ type: 'UPDATE', id: this.props.post.id, data: data })
    }
    render() {
        return (
            <div key={this.props.post.id} className="post">
                <div class="card">
                    <div class="card-body">
                        <form onSubmit={this.handleEdit}>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Title</label>
                                <input required type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukan Title" ref={(input) => this.getTitle = input}
                                    defaultValue={this.props.post.title} />
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Deskriptsi</label>
                                <textarea required class="form-control" rows="5" ref={(input) => this.getMessage = input}
                                    defaultValue={this.props.post.message}></textarea>
                            </div>
                            <button className="btn btn-primary btn-block">
                                UPDATE
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}
export default connect()(EditComponent);
import React, { Component } from 'react';
import Headercontent from '../Components/HeaderContent';

class Content extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            textarea: 'Ini Text Areaku',
            errormessage: <strong>Mohon Diisi Passowrdnya</strong>
        };
    }

    myChangeHandler = (event) => {
        let nam = event.target.name;
        let val = event.target.value;
        let err = '';
        console.log(nam);
        console.log(val);
        if (nam === "password") {
            if (val) {
                err = ''
                this.setState({ errormessage: err });
            }
        }
        this.setState({
            [nam]: val
        });
    }

    render() {
        const { name } = this.props;
        return (
            <section className="content">
                <div className="row">
                    <Headercontent name={name} />
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <div className="card card-primary">
                            <div className="card-header">
                                <h3 className="card-title">Quick Example</h3>
                            </div>
                            <form>
                                <div className="card-body">
                                    <div className="form-group">
                                        <label>Email address :  {this.state.email}</label>
                                        <input type="email" name="email" className="form-control"
                                            onChange={this.myChangeHandler} placeholder="Enter email" />
                                    </div>
                                    <div className="form-group">
                                        <label>Password
                                            : {this.state.password}<small>{this.state.errormessage}</small></label>
                                        <input type="password" name="password" className="form-control" onChange={this.myChangeHandler} placeholder="Password" />
                                    </div>
                                    <div className="form-group">
                                        <label>Text Area </label>
                                        <textarea name="textarea" className="form-control" value={this.state.textarea} onChange={this.myChangeHandler}></textarea>
                                    </div>
                                </div>
                                <div className="card-footer">
                                    <button type="button" className="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default Content;